package net.dreamlu.test;

import java.io.IOException;

import net.dreamlu.assets.kit.AssetsKit;

/**
 * 
 * @author lcm
 *
 */
public class AssetsTest {

	public static void main(String[] args) throws IOException {
//		String filejs = "classpath:net/dreamlu/test/assets/assets-web.jjs";
//		System.out.println(AssetsKit.getPath(filejs));
		
		String filecss = "classpath:net/dreamlu/test/assets/assets-web.jcss";
		System.out.println(AssetsKit.getPath(filecss));
	}
}
