## 说明
JFinal框架结合JFinal3.0 template、jsp、beetl、freemarker模版的js、css在线合并压缩插件！

结合CDN使用效果更佳哦~

## 依赖
1. JFinal

2. yuicompressor

3. commons-io

## 使用
```
<dependency>
    <groupId>net.dreamlu</groupId>
    <artifactId>JFinal-assets</artifactId>
    <version>1.2.0</version>
</dependency>
```
###JFinal3.0 template中使用
###自定义指令
```
me.addDirective("assets", new AssetsDirective());
```
###js
```
<script src='#assets("/assets/assets.jjs")'></script>
```
###css
```
<link rel="stylesheet" href='#assets("/assets/assets.jcss")'/>
```

assets.jjs示例：
```
#开头表注释
/js/jquery.min.js
/js/jquery-ui.min.js
/js/modernizr.min.js
/js/superfish.min.js
/js/application.js
```
目录相对于WebRoot、webapp的目录

###Beetl中使用
###自定义标签
```
##自定义标签
TAG.assets = net.dreamlu.ui.beetl.AssetsTag
```
###js
```
<#assets file="/assets/assets.jjs"; src>
    <script src="${ctxPath}${src}"></script>
</#assets>
```
###css
```
<#assets file="/assets/assets.jcss"; href>
    <link rel="stylesheet" href="${ctxPath}${href}"/>
</#assets>
```

###JSP中使用

首先、导入标签库
```
<%@ taglib prefix="assets" uri="http://www.dreamlu.net/tags/assets.tld" %>
```

同理如beetl
```
<assets:assets var="x" file="/assets/assets.jjs">
	<script src="${x}" type="text/javascript" ></script>
</assets:assets>
```
###freemarker中使用

首先、配置（可在JFinal的config中完成）
``` 
FreeMarkerRender.getConfiguration().setSharedVariable("assets", new AssetsDirective());
```

同理如beetl
```
<@assets var="x" file="/assets/assets.jjs">
	<script src="${x}" type="text/javascript" ></script>
</@assets>
```

## 文章
[对css，js压缩之combo以及七牛cdn的思考:http://blog.dreamlu.net/blog/47](http://blog.dreamlu.net/blog/47)

## 更新说明
>## 2017-11-25 v1.3.0
支持JFinal3.3，配置添加路径判断区分绝对路径、相对路径、classpath、webjars等。

1. 绝对路径    c://xxx/xxx
2. 相对路径    static/xxx/xx
3. classpath classpath:net/dreamlu/assets/test.js
4. webjars   webjars:/webjars/jquery/3.1.1-1/jquery.min.js
5. http https

TODO：对于http、classpath、webjars的css合并压缩的css img路径能会合并出问题

>## 2017-03-08 v1.2.0
>1. 升级到jfinal3.0，添加JFinal3.0 template的支持！

>## 2016-11-08 v1.1.0
>1. 修改注释行`#`号照成生成空文件的bug。

>## 2015-12-30 v1.0.0
>1. 升级到JFinal2.1，JFinal低版本用户请使用`v0.0.3`

## 交流群
如梦技术：[`237587118`](http://shang.qq.com/wpa/qunwpa?idkey=f78fcb750b4f72c92ff4d375d2884dd69b552301a1f2681af956bd32700eb2c0)

## 捐助共勉
<img src="http://soft.dreamlu.net/weixin-9.jpg" width = "200" alt="微信捐助" align=center />
<img src="http://soft.dreamlu.net/alipay.png" width = "200" alt="支付宝捐助" align=center />
<img src="http://soft.dreamlu.net/qq-9.jpg" width = "200" alt="QQ捐助" align=center />

## License

( The MIT License )